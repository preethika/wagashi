class Purchase < ApplicationRecord
  has_many :products_purchases, dependent: :destroy
  has_many :products, through: :products_purchases
  belongs_to :customer

  include DiscountsHelper

  def place_a_purchase(product_inputs)
    # process raw list
    purchased_products = []
    product_inputs.map do |product|
      product[:quantity].to_i.times do
        purchased_products << { product_id: product[:id].to_i, charged: 0.00, discount_id: nil }
      end
    end

    inventory_products = Product.all
    discounts = self.customer.discounts.where(status: DISCOUNT_STATUS["active"])
    discounted_products = []
    deduct_items = []
    remaining_items = purchased_products

    if (!discounts.blank?)
      # filter all discounts by item quantity
      discounts_by_quantity = discounts.select { |discount| discount.discount_type == DISCOUNT_TYPE["free item reduction (units)"] }
      discounts_by_quantity.each do |discount|
        discounted_quantity = purchased_products.select{ |item| item[:product_id] == discount.product_id }.count
        discounted_quantity /= discount.minimum_purchase if discount.minimum_purchase > 0

        next if discounted_quantity == 0

        discounted_quantity.times do
          remaining_items.delete_at(remaining_items.index{ |item| item[:product_id] == discount.product_id || remaining_items.length } )
          deduct_items << { product_id: discount.product_id, charged: 0.00, discount_id: DISCOUNT_TYPE["free item reduction (units)"] }
        end
      end

      # adjust pricing align to discount pricing
      discounts_by_pricing = discounts.select { |discount| discount.discount_type == DISCOUNT_TYPE["price reduction (%)"] }
      inventory_products.each_with_index do |inventory_product, index|
        discounts = discounts_by_pricing.select { |discount| discount if (discount.product_id == inventory_product.id) }
        next if (discounts.blank?)
        last_discount = discounts.last
        applicable_items = remaining_items.select { |item| item[:product_id] == last_discount.product_id }
        if (applicable_items.count >= last_discount.minimum_purchase)
          inventory_products[index].price = inventory_product.price * (1.00 - (last_discount.rate / 100.00))
          discounted_products << inventory_products[index]
        end
      end
    end

    # process items lists
    total_items = []
    remaining_items.each do |item|
      product = inventory_products.select{ |product| product.id == item[:product_id] }.last
      any_discounts = discounted_products.select{ |discounted_product| discounted_product.id == product.id }
      if (any_discounts.blank?)
        total_items << { product_id: item[:product_id], charged: product.price }
      else
        total_items << { product_id: item[:product_id], charged: product.price, discount_id: any_discounts.last.id }
      end
    end

    # calculate total price
    self.total = 0.00
    total_items.each do |price_item|
      self.total += price_item[:charged]
    end

    # add all previously discounted items back to list
    deduct_items.each do |item|
      total_items << item
    end

    # save all total items into database
    self.products_purchases.create(total_items)

  end
end
