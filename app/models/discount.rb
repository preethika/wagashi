class Discount < ApplicationRecord
  belongs_to :product
  belongs_to :customer
  has_many :products_purchases

  validates :product_id, presence: true
  validates :customer_id, presence: true
  validates :discount_type, presence: true
  validates :minimum_purchase, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :rate, presence: true, numericality: { greater_than_or_equal_to: 0 }
  validates :status, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
